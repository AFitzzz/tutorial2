<%@page import="com.simonwoodworth.mavenproject5.Calculator"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Calculator</title>
    </head>
    <body>
        <h1>Hello IS3313!</h1>
        <p>A simple calculation</p>
        <p><%= Calculator.add(3, 4)%></p>
        <p><%= Calculator.subtract(7,3)%></p>
        <p><%= Calculator.multiply(7,3)%></p>
        <p><%= Calculator.divide(9,3)%></p>
        <p><%= Calculator.square(4)%></p>
        <p><%= Calculator.remainder(10,100)%></p>
        <p><%= Calculator.cube(4)%></p>
        <!<!-- Testing for GitLab commit -->
    </body>
    ,,,
</html>