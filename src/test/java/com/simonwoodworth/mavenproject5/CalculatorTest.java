/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.simonwoodworth.mavenproject5;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author simon
 */
public class CalculatorTest {
    
    public CalculatorTest() {
    }

    @Test
    public void testAdd() {
        assertEquals(9, Calculator.add(4, 5));
    }
    
     @Test
    public void testSubtract() {
        assertEquals(7, Calculator.subtract(12, 5));
    }
    
     @Test
    public void testMultiply() {
        assertEquals(24, Calculator.multiply(6, 4));
    }
    
    @Test
    public void testDivide() {
        assertEquals(5, Calculator.divide(20, 4));
    }
    
    @Test
    public void testSquare() {
        assertEquals(16, Calculator.square(4));
    }
    
    @Test
    public void testRemainder() {
        assertEquals(10, Calculator.remainder(10, 100));
    }
    
    @Test
    public void testCube() {
        assertEquals(64, Calculator.cube(4));
    }
    
}
